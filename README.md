# Range

A library to implement integer ranges.

## TODO

  * Surely there are bugs
  * I have to write documentation
  * I forgot to write `has`, `asList` and `iterator`
  * It's open-source, have fun!
  
## What is an integer range?

An integer range is a collection of numbers, not necessarily sequential, but usefully so.
One can add and remove single digits or sequential ranges, and query for the presence of
a specific integer.

## Example

```java
		Range r = new Range(1, 15);
		r.add(20, 25);
		r.remove(16, 22);
        assert !r.has(16);
        assert !r.has(17);
        assert !r.has(18);
        assert !r.has(19);
        // etc
        assert !r.has(22);
        
        assert r.has(23);
        assert r.has(24);
        assert r.has(25);
```

## Documentation

See [https://tripleo.gitlab.io/range/](https://tripleo.gitlab.io/range/)

## License

    Apache 2.0
