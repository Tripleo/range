/*
 * Tripleo-Range, copyright Tripleo <oluoluolu+range-impl@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.range;

import org.junit.Assert;
import org.junit.Test;

public class RangeRemovalTest {


	@Test
	public void emptyRangeRemovalIsEmpty() {
		Range r = new Range();
		r.remove(1);
		Assert.assertEquals("Removal from Empty Range no-op", "(end)", r.lispRepr());
	}

	@Test
	public void removalFromDualContinuousRanges() {
		Range r = new Range(1, 15);
		r.add(17, 20);
		r.remove(18);
		Assert.assertEquals("Removal From Dual Continuous Ranges Shows Properly", "(c 1 15)(s 17)(c 19 20)(end)", r.lispRepr());
	}

	@Test
	public void removalFromOverlappingRanges() {
		Range r = new Range(1, 15);
		r.add(20, 25);
		r.remove(12, 22);
		Assert.assertEquals("Removal From Overlapping Ranges Shows Properly", "(c 1 11)(c 23 25)(end)", r.lispRepr());
	}

	@Test
	public void removalFromNonOverlappingRanges() {
		Range r = new Range(1, 15);
		r.add(20, 25);
		r.remove(16, 22);
		Assert.assertEquals("Removal From Non-Overlapping Ranges Shows Properly", "(c 1 15)(c 23 25)(end)", r.lispRepr());
	}

/*
	@Test
	public void continuousRangeWithOneAddedAtEndShowsProperly() {
		Range r = new Range(1, 15);
		r.add(16);
		Assert.assertEquals("Continuous Range With One Added At EndS hows Properly", "(c 1 16)(end)", r.listRepr());
	}

	@Test
	public void continuousRangeWithOneAddedAtEndShowsProperlyReversed() {
		Range r = new Range();
		r.add(16);
		r.add(1, 15);
		Assert.assertEquals("16 + 1,15", "(c 1 16)(end)", r.listRepr());
	}
*/

	@Test
	public void removeRange() {
		Range r = new Range(1, 15);
		r.remove(6, 9);
		Assert.assertEquals("1,15 - 6,9", "(c 1 5)(c 10 15)(end)", r.lispRepr());
	}

	@Test
	public void removeSingle() {
		Range r = new Range(1, 5);
		r.remove(4);
		Assert.assertEquals("Remove Single", "(c 1 3)(s 5)(end)", r.lispRepr());
	}

}

//
//
//
