/*
 * Tripleo-Range, copyright Tripleo <oluoluolu+range-impl@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.range;

import org.junit.Assert;
import org.junit.Test;

public class RangeTest {

	@Test
	public void emptyRangeIsEmpty() {
		Range r = new Range();
		Assert.assertEquals("Empty Range is Empty", "(end)", r.lispRepr());
	}

	@Test
	public void continuousRangeShowsProperly() {
		Range r = new Range(1, 15);
		Assert.assertEquals("Continuous Range Shows Properly", "(c 1 15)(end)", r.lispRepr());
	}

	@Test
	public void continuousRangeWithOneAddedAtEndShowsProperly() {
		Range r = new Range(1, 15);
		r.add(16);
		Assert.assertEquals("Continuous Range With One Added At End Shows Properly", "(c 1 16)(end)", r.lispRepr());
	}

	@Test
	public void continuousRangeWithOneAddedInMiddleShowsProperly() {
		Range r = new Range(1, 15);
		r.add(12);
		Assert.assertEquals("Continuous Range With One Added In Middle Shows Properly", "(c 1 15)(end)", r.lispRepr());
	}

	@Test
	public void continuousRangeWithOneAddedAtEndShowsProperlyReversed() {
		Range r = new Range();
		r.add(16);
		r.add(1, 15);
		Assert.assertEquals("16 + 1,15", "(c 1 16)(end)", r.lispRepr());
	}

	@Test
	public void addBetweenTwoSeparateRanges() {
		Range r = new Range(1, 5);
		r.add(10, 15);
		r.add(6, 9);
		Assert.assertEquals("Add Between 2 separate Ranges shows properly", "(c 1 15)(end)", r.lispRepr());
	}

	@Test
	public void twoSeparateRanges() {
		Range r = new Range(1, 5);
		r.add(10, 15);
		Assert.assertEquals("2 separate Ranges shows properly", "(c 1 5)(c 10 15)(end)", r.lispRepr());
	}

	@Test
	public void twoOverlappingRanges() {
		Range r = new Range(1, 15);
		r.add(11, 25);
		Assert.assertEquals("2 Overlapping Ranges shows properly", "(c 1 25)(end)", r.lispRepr());
	}

}

//
//
//
