/*
 * Tripleo-Range, copyright Tripleo <oluoluolu+range-impl@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.range;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created 9/20/20 12:10 AM
 */
public class RangeToStringTests {

	@Test
	public void addBetweenTwoSeparateRangesToString() {
		Range r = new Range(1, 5);
		r.add(10, 15);
		r.add(6, 9);
		Assert.assertEquals("Add Between 2 separate Ranges shows properly", "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, ", r.toString());
	}

}

//
//
//
